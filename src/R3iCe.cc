#include "rheolef.h"
#include "utils/include/cxxopts.hpp"
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <math.h>
#include <nlohmann/json.hpp>
#include <boost/algorithm/string.hpp>
#include <tuple>

//for patch rhori import
//----------------------------
#include <fcntl.h>  
#include <sys/file.h>  
#include <unistd.h>  
//----------------------------


using namespace rheolef;
using namespace std;

using json = nlohmann::json;


#include "utils/json/json.icc"
#include "utils/parameter/parameter.icc"
#include "utils/boundary/boundary_dirichlet.icc"
#include "utils/boundary/boundary_neumann.icc"
#include "utils/generate_orientation/utils_ori.icc"
#include "utils/generate_orientation/loading_ori.icc"
#include "utils/generate_orientation/generate_orientation.icc"
#include "utils/algebra/algebra_function.icc"
#include "utils/CTI_equation/non_linear.icc"
#include "utils/CTI_equation/CTI_direct.icc"
#include "utils/c_evolution/find_c0.icc"
#include "utils/c_evolution/c_evolution.icc"



int main(int argc, char**argv) {
    environment rheolef(argc, argv);
    
    // Read option
    cxxopts::Options options("Ice Modelling", "Resolve CTI law using finite element method");
    options.add_options()
        // CTI law parameter
        // input file option
        ("j,json", "use json file to run experiment", cxxopts::value<string>()->default_value("none"))
        ("r,restart","restart simulation", cxxopts::value<bool>()->default_value("false"))
        ("v,version", "print version", cxxopts::value<bool>()->default_value("false"))
        ;

    auto param_simu = options.parse(argc, argv);

    if (param_simu["version"].as<bool>()){
    
        cout << "\n .---------------------------------------------------------.";
        cout << "\n |                                                         |";
        cout << "\n |                                                         |";
        cout << "\n |   ███████████    ████████   ███    █████████            |";
        cout << "\n |  ░░███░░░░░███  ███░░░░███ ░░░    ███░░░░░███           |";
        cout << "\n |   ░███    ░███ ░░░    ░███ ████  ███     ░░░   ██████   |";
        cout << "\n |   ░██████████     ██████░ ░░███ ░███          ███░░███  |";
        cout << "\n |   ░███░░░░░███   ░░░░░░███ ░███ ░███         ░███████   |";
        cout << "\n |   ░███    ░███  ███   ░███ ░███ ░░███     ███░███░░░    |";
        cout << "\n |   █████   █████░░████████  █████ ░░█████████ ░░██████   |";
        cout << "\n |  ░░░░░   ░░░░░  ░░░░░░░░  ░░░░░   ░░░░░░░░░   ░░░░░░    |";
        cout << "\n |                                                         |";
        cout << "\n |                                                         |";
        cout << "\n '---------------------------------------------------------'";
        cout << "\n";
        cout << "\n Finite element code to solve texture evolution in transverse isotropic material such a ice Ih.";
        cout << "\n version : 0.9.1";
        cout << "\n\n doc : https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/ice-polycrystal-models/ipms_documentation/docs/Rheolef_doc/rheolef_main.html";
        cout << "\n git: https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti";
        cout << "\n doi: https://doi.org/10.5802/crmeca.243";
        cout << "\n\n contact : Thomas Chauve";
        cout << "\n mail : thomas.chauve@univ-grenoble-alpes.fr\n";
        
        return 0;
    }

    // Print to know the number of thread
    cout << "+1 core\n";
    
    // Load the parameters for the simulation
    json data_simu;
    if (param_simu["json"].as<string>()=="none"){
        cout << "No input file provide. Please provide one.\n";
        return 0;
    }
    else{
        string json_file = param_simu["json"].as<string>();
        cout << "Loading : from json file, " + json_file + "\n";
        data_simu = importJson(json_file);
    };
   
    odiststream file_simu (data_simu["output"]["out_folder"].get<string>()+"simu", "out", io::app);

    file_simu << "\n##################################\n";
    file_simu << "### Welcome to ice CTI solver ####\n";
    file_simu << "##################################\n\n";

    // print the simulation parameters
    printJson(data_simu);

    // load mesh
   
    file_simu << "\n##################################\n";
    file_simu << "########### Load mesh ############\n";
    file_simu << "##################################\n";
    
    geo omega (data_simu["input"]["mesh_file"].get<string>());

    // get the mesh dimension
    size_t d = omega.dimension();
    file_simu << "Mesh dimension : ";
    file_simu << d;
    file_simu << "D\n";
    // identity tensor
    tensor Id = tensor::eye(d);

    // generate space
    space Xh=block_boundary(data_simu["solver"]["u_order"].get<string>(),omega,data_simu);  
    space Qh (omega, data_simu["solver"]["p_order"].get<string>());
    space Th (omega, data_simu["solver"]["p_order"].get<string>(), "tensor");
    space Sh (omega, data_simu["solver"]["o_order"].get<string>());
    space Vh (omega, data_simu["solver"]["o_order"].get<string>(), "vector");
    space Mh (omega, data_simu["solver"]["o_order"].get<string>(), "tensor");

    file_simu << "\n##################################\n";
    file_simu << "######### Output files ###########\n";
    file_simu << "##################################\n";

    // backup_file
    string backup_file = data_simu["output"]["out_folder"].get<string>()+"backup";

    // generate output file
    odiststream out_scalar_P0(data_simu["output"]["out_folder"].get<string>()+"scalar_"+data_simu["solver"]["o_order"].get<string>(), "branch", io::app);
    branch event_scalar_P0("t","eta_ice","tr(D)","tr(S)","norm(D)","norm(S)","RX_speed","work_rate");
    odiststream out_scalar_P1(data_simu["output"]["out_folder"].get<string>()+"scalar_"+data_simu["solver"]["p_order"].get<string>(), "branch", io::app);
    branch event_scalar_P1("t","pressure");
    odiststream out_vector_P0(data_simu["output"]["out_folder"].get<string>()+"vector_"+data_simu["solver"]["o_order"].get<string>(), "branch", io::app);
    branch event_vector_P0("t","c-axis","c0","cRot","cRX","cDef");
    odiststream out_tensor_P0(data_simu["output"]["out_folder"].get<string>()+"tensor_"+data_simu["solver"]["o_order"].get<string>(), "branch", io::app);
    branch event_tensor_P0("t","M");
    odiststream out_tensor_P1(data_simu["output"]["out_folder"].get<string>()+"tensor_"+data_simu["solver"]["p_order"].get<string>(), "branch", io::app);
    branch event_tensor_P1("t","D","S","sigma");
    odiststream out_vector_P2(data_simu["output"]["out_folder"].get<string>()+"vector_"+data_simu["solver"]["u_order"].get<string>(), "branch", io::app);
    branch event_vector_P2("t","uh");

    // generate text file
    odiststream file_nl_conv (data_simu["output"]["out_folder"].get<string>()+"fix_point_nl", "txt", io::app);
    odiststream file_time_all (data_simu["output"]["out_folder"].get<string>()+"time_step", "txt", io::app);
    odiststream file_c_conv (data_simu["output"]["out_folder"].get<string>()+"fix_point_c", "txt", io::app);
    file_nl_conv.close();
    file_c_conv.close();
    //file_time_all.close();

    // generate orientation
    field c_axis(Vh,0.0);
    c_axis = generate_orientation(omega,Sh,Vh,data_simu);

    // extract physical value for CTI law
    double a1,a2,a3;
    tie(a1, a2, a3) = cti_parameters(data_simu);
    
    // For rheolef magic 
    trial u(Xh), p(Qh); test v(Xh), q(Qh); 
    trial lambda(Th); test tau(Th); 
    // for residual computation
    form mt = integrate(ddot(lambda,tau)); // to compute stopping criterions on stress
    //Define quadrature type for summations and averaging through integrate function
    quadrature_option_type qopt;
    qopt.set_family(quadrature_option_type::gauss);
    qopt.set_order(1);

    field a_(Sh, 1.0);
    double area_tot = integrate(omega, a_, qopt);
    
    double area_tmp;
    double area_TOP = integrate(omega["top"], a_, qopt);
    double area_BOTTOM = integrate(omega["bottom"], a_, qopt);
    double area_LEFT = integrate(omega["left"], a_, qopt);
    double area_RIGHT = integrate(omega["right"], a_, qopt);
    double area_FRONT = 1;
    double area_BACK = 1;
    if (d==3){
        area_FRONT = integrate(omega["front"], a_, qopt);
        area_RIGHT = integrate(omega["right"], a_, qopt);   
    }
    point size_mesh=omega.xmax()-omega.xmin();


    file_simu << "\n##################################\n";
    file_simu << "######### Applied force ##########\n";
    file_simu << "##################################\n";

    file_simu << "Area TOP:";
    file_simu << area_TOP;
    file_simu << "\n";


    // rheolef magic : neumann boundary condition
    field lh;
    field lh_tmp;
    lh=field(Xh ,0);
    

    std::vector<std::string> directions = {"top", "bottom", "left", "right", "front", "back"};

    // Access and print the elements of the vector
    for (const std::string& direction : directions) {
        if (data_simu["bc"][direction]["type_x"].get<string>()=="neumann" || data_simu["bc"][direction]["type_y"].get<string>()=="neumann" || data_simu["bc"][direction]["type_z"].get<string>()=="neumann"){
            point force;
            area_tmp = integrate(omega[direction], a_, qopt);
            force = neumann_bc(area_tmp,direction,data_simu);
            file_simu << "Force ";
            file_simu << direction;
            file_simu << ":\n";
            file_simu << force;
            file_simu << "\n";
            lh_tmp = integrate(omega[direction],dot(force, v));
            lh = interpolate(Xh,lh+lh_tmp);
        }
    }


    // field for the fix point
    int k_nl=0;
    int k_c=0;
    int k_c_lim=data_simu["time_step"]["it_max_c_0"].get<int>();
    
    field eta_nl(Sh,1.0);
    
    field uh = boundary_condition(Xh, data_simu);

    field ph(Qh,0);

    field sigma_c(Th, 0.0); // sigma_c = S_c+ph*Id
    field S_c(Th, 0.0);
    field S_c_1(Th, 0.0);
    field S_nl(Th, 0.0);
    field S_nl_1(Th, 0.0);

    field c_axis_1(Vh,0.0);
    field c_axis_2(Vh,0.0);
    c_axis_1=c_axis;
    c_axis_2=c_axis;
    
    field M(Mh, 0.0);
    M = interpolate(Mh,compose(VDiadic,c_axis,c_axis));
    
    double residual_S_nl=1.0;
    double residual_S_c=1.0;

    // field for ouput
    field strain_rate(Th,0.0);
    field trD(Sh, 0.0);
    field trS(Sh, 0.0);
    field work(Sh,0.0);
    field normD(Sh, 0.0);
    field normS(Sh, 0.0);
    field c0(Vh,0.0);
    field cRot(Vh,0.0);
    field cDef(Vh,0.0);
    field cRX(Vh,0.0);
    field RX_speed(Sh,0.0);

    field dij_f(Sh, 0.0);
    field uhi;

    // scalar for output
    double average_trD = 0.0;
    double average_trS = 0.0;
    double average_normD = 0.0;
    double average_normS = 0.0;
    double d11=0.0; double d12=0.0; double d13=0.0; double d22=0.0; double d23=0.0; double d33=0.0;
    double s11=0.0; double s12=0.0; double s13=0.0; double s22=0.0; double s23=0.0; double s33=0.0;
    double d11m=0.0; double d12m=0.0; double d13m=0.0; double d22m=0.0; double d23m=0.0; double d33m=0.0; double d21m=0.0; double d31m=0.0; double d32m=0.0;
    //point 

    int t=0;
    if (param_simu["restart"].as<bool>()){
        idiststream input (backup_file, "field");
        input >> catchmark("t") >> t
              >> catchmark("uh") >> uh
              >> catchmark("ph") >> ph
              >> catchmark("c_axis") 	>> c_axis 
              >> catchmark("c_axis_1") 	>> c_axis_1
              >> catchmark("c_axis_2")	>> c_axis_2;
        input.close();
        // TO DO purge output file from not needed line
    }
    else{
        //file_time_all.open(data_simu["output"]["out_folder"].get<string>()+"time_step", "txt", io::app);
        file_nl_conv.open(data_simu["output"]["out_folder"].get<string>()+"fix_point_nl", "txt", io::app);
        file_c_conv.open(data_simu["output"]["out_folder"].get<string>()+"fix_point_c", "txt", io::app);

        file_nl_conv << "time_iter" << ";" << "k_c" << ";" << "k_nl" << ";" << "residual_S_nl"  << ";" << "runtime_nl_loop"<< ";" << "sopt_residue";
        file_c_conv << "time_iter" << ";" << "k_c" << ";" << "k_nl_max" << ";" << "residual_S_c" << ";" << "runtime_c_loop";

        file_time_all << "time_iter" << ";" << "k_c_max" << ";" << "k_nl_max" << ";" << "residual_S_c" << ";" << "trD_mean" << ";" 
                      << "trS_mean" << ";" << "normD_mean" << ";" << "normS_mean" 
                      << ";" << "d11" << ";" << "d12" << ";" << "d13" << ";" << "d22" << ";" << "d23" << ";" << "d33"
                      << ";" << "s11" << ";" << "s12" << ";" << "s13" << ";" << "s22" << ";" << "s23" << ";" << "s33"
                      << ";" << "d11m" << ";" << "d12m" << ";" << "d13m" << ";" << "d21m" << ";" << "d22m" << ";" << "d23m" << ";" << "d31m" << ";" << "d32m" << ";" << "d33m"
                      << ";" << "stx" << ";" << "sty" << ";" << "stz"
                      << ";" << "sbox" << ";" << "sboy" << ";" << "sboz"
                      << ";" << "srx" << ";" << "sry" << ";" << "srz"
                      << ";" << "slx" << ";" << "sly" << ";" << "slz"
                      << ";" << "sfx" << ";" << "sfy" << ";" << "sfz"
                      << ";" << "sbax" << ";" << "sbay" << ";" << "sbaz"                  
                      << ";" << "runtime";

        file_nl_conv.close();
        file_c_conv.close();

        out_scalar_P0 << event_scalar_P0(t,eta_nl,trD,trS,normD,normS,RX_speed,work);
        out_scalar_P1 << event_scalar_P1(t,ph);
        out_vector_P0 << event_vector_P0(t,c_axis,c0,cRot,cRX,cDef);
        out_tensor_P0 << event_tensor_P0(t,M);
        out_tensor_P1 << event_tensor_P1(t,strain_rate,S_c,sigma_c);
        out_vector_P2 << event_vector_P2(t,uh);
    };
    //file_time_all.close();
    
    odiststream backup(backup_file, "field");
    
    // ##########################
    // Start of the time loop (t)
    // ##########################
    do{
        std::clock_t t_start = std::clock();
        ++t;
        k_c=0;
        c_axis_2=c_axis_1;
        c_axis_1=c_axis;
        do{// loop on c 
            std::clock_t c_start = std::clock();
            ++k_c;
            k_nl=0;
            do{// loop on nl
                std::clock_t nl_start = std::clock();

                ++k_nl;
                cout << "\n t: " + to_string(t)+" k_c: "+to_string(k_c)+" k_nl: "+ to_string(k_nl);
                M = interpolate(Mh,compose(VDiadic,c_axis,c_axis));

                
                // rheolef magic
                form  a  = integrate (2.0*eta_nl*(2.0*a1*ddot(D(u), D(v))
                                              +  2.0*a2*tr(M*D(u))*ddot(M,D(v))
                                              -  2.0/d*a2*tr(M*D(u))*div(v)
                                              +  a3*ddot(M*D(u), D(v)) 
                                              +  a3*ddot(D(u)*M, D(v))
                                              -  2.0/d*a3*tr(M*D(u))*div(v)
                                              ));

                form b = integrate(-div(u)*q);
                // solver opt : need to be inside the loop as sopt value are change in each stokes.solves run
                solver_option sopt;
                sopt.iterative=data_simu["solver"]["iterative"].get<bool>();
                // sopt.iterative need to be run with more option (restriction 06/06/2024 - may be fix later) ask Pierre Saramito for detail
                problem inner_pa (a,sopt);
                form mp = integrate(p*q);
                problem inner_pm(mp,sopt);
                // Build the probleme
                problem_mixed stokes (a,b,sopt);
                // sopt.iterative need to be run with more option (restriction 06/06/2024 - may be fix later) ask Pierre Saramito for detail
                stokes.set_inner_problem(inner_pa);
                stokes.set_preconditionner(inner_pm);
                // Solve the stokes problem           
                stokes.solve(lh, field(Qh,0), uh, ph);
                
                file_simu.open(data_simu["output"]["out_folder"].get<string>()+"simu", "out", io::app);
                file_simu << "\n t: " + to_string(t)+" k_c: "+to_string(k_c)+" k_nl: "+ to_string(k_nl);

                // end rheolef magic
                if (data_simu["non_linear"]["non_linear"].get<bool>()){
                    eta_nl=non_linear_viscosity(uh,c_axis,Sh,Th,Mh,data_simu);
                }
                S_nl_1=S_nl;
                S_nl = stress_CTI_direct(uh,c_axis,Sh,Th,Mh,data_simu);
                field eh_res = S_nl-S_nl_1;
                residual_S_nl  = sqrt(mt(eh_res,eh_res)*sqr(1/area_tot));
                file_simu << " residual nl loop: "+to_string(residual_S_nl);
                cout << " residual nl loop: "+to_string(residual_S_nl);
                
                std::clock_t nl_end = std::clock();
                Float d2 = 1000 * (nl_end-nl_start)/ CLOCKS_PER_SEC;
                
                file_nl_conv.open(data_simu["output"]["out_folder"].get<string>()+"fix_point_nl", "txt", io::app);
                file_nl_conv << t << ";" << k_c << ";" << k_nl << ";" << residual_S_nl << ";" << d2 << ";" << sopt.residue ;
                file_nl_conv.close();

                if (k_nl>=data_simu["non_linear"]["it_max_nl"].get<int>() || residual_S_nl < data_simu["non_linear"]["tol_nl"].get<double>()  || !data_simu["non_linear"]["non_linear"].get<bool>()) break;
            }while(true);
            if (!data_simu["time_step"]["no_time"].get<bool>()){
                // Note choose BDFi, BDF2 should be better but I implement BDF1 for test. No input option to select which BDFi but It can be added if necessairy.
                tie(c_axis, c0)=c_axis_BDF2(uh,c_axis_1,c_axis_2,c_axis,Sh,Vh,Th,Mh,data_simu); // BDF2 formulation
                //tie(c_axis, c0)=c_axis_BDF1(uh,c_axis_1,c_axis,Sh,Vh,Th,Mh,data_simu); // BDF1 formulation
                S_c_1=S_c;
                S_c = stress_CTI_direct(uh,c_axis,Sh,Th,Mh,data_simu);
                field eh_res = S_c-S_c_1;
                residual_S_c  = sqrt(mt(eh_res,eh_res)*sqr(1/area_tot));

                std::clock_t c_end = std::clock();
                Float d1 = 1000 * (c_end-c_start)/ CLOCKS_PER_SEC;

                file_simu << " residual c loop: "+to_string(residual_S_c);
                cout << " residual c loop: "+to_string(residual_S_c);

                file_c_conv.open(data_simu["output"]["out_folder"].get<string>()+"fix_point_c", "txt", io::app);
                file_c_conv << t << ";" << k_c << ";" << k_nl << ";" << residual_S_c << ";" << d1 ;
                file_c_conv.close();
            }
            else{
                S_c = stress_CTI_direct(uh,c_axis,Sh,Th,Mh,data_simu);
                tie(std::ignore, c0)=c_axis_BDF2(uh,c_axis_1,c_axis_2,c_axis,Sh,Vh,Th,Mh,data_simu);
            }
            if (k_c>=k_c_lim || residual_S_c < data_simu["time_step"]["tol_c"].get<double>() || data_simu["time_step"]["no_time"].get<bool>()) break;
        }while(true);
        // change the number of iteration maximal
        k_c_lim=data_simu["time_step"]["it_max_c"].get<int>();
        // export output file
        // Compute stress field sigma. The first line doesn't work. I don't understand why therefore I add the pressure on each diagonal components.
        // sigma_c = interpolate(Th,S_c+1./3.*Id*ph);
        sigma_c = S_c;
        sigma_c(0,0)=interpolate(Qh,sigma_c(0,0)+1./d*ph);
        sigma_c(1,1)=interpolate(Qh,sigma_c(1,1)+1./d*ph);
        if (d==3){
            sigma_c(2,2)=interpolate(Qh,sigma_c(2,2)+1./d*ph);
        }

        M = interpolate(Mh,compose(VDiadic,c_axis,c_axis));
        strain_rate = interpolate(Th,D(uh));
        trD = interpolate(Sh,tr(strain_rate));
        trS = interpolate(Sh,tr(S_c));
        average_trD = abs(integrate(omega, trD, qopt));
        average_trS = abs(integrate(omega, trS, qopt));
        normD = interpolate(Sh,norm(strain_rate));
        average_normD = abs(integrate(omega, normD, qopt));
        normS = interpolate(Sh,norm(S_c));
        average_normS = abs(integrate(omega, normS, qopt));
        cRot=c_axis-c_axis_1;
        cRX=(c0-c_axis_1);
        cDef=interpolate(Vh,((grad(uh)-trans(grad(uh)))/2.0)*c_axis - data_simu["time_step"]["lambda"].get<double>()*(strain_rate*c_axis-dot(c_axis,strain_rate*c_axis)*c_axis));
        work=interpolate(Sh,ddot(strain_rate,S_c));
        RX_speed=interpolate(Sh,norm(cRX));
        
        if (data_simu["non_linear"]["non_linear"].get<bool>()){
            eta_nl=non_linear_viscosity(uh,c_axis,Sh,Th,Mh,data_simu);
        }

        if (t % data_simu["output"]["out_modulo"].get<int>() == 0 || data_simu["time_step"]["no_time"].get<bool>()){
            out_scalar_P0 << event_scalar_P0(t,eta_nl,trD,trS,normD,normS,RX_speed,work);
            out_scalar_P1 << event_scalar_P1(t,ph);
            out_vector_P0 << event_vector_P0(t,c_axis,c0,cRot,cRX,cDef);
            out_tensor_P0 << event_tensor_P0(t,M);
            out_tensor_P1 << event_tensor_P1(t,strain_rate,S_c,sigma_c);
            out_vector_P2 << event_vector_P2(t,uh);
        }
            
        // extract macro components from D and S 
        dij_f=strain_rate(0,0); d11 = integrate(omega, dij_f, qopt)/area_tot;
        dij_f=strain_rate(0,1); d12 = integrate(omega, dij_f, qopt)/area_tot;
        dij_f=strain_rate(1,1); d22 = integrate(omega, dij_f, qopt)/area_tot;
        
        dij_f=sigma_c(0,0); s11 = integrate(omega, dij_f, qopt)/area_tot;
        dij_f=sigma_c(0,1); s12 = integrate(omega, dij_f, qopt)/area_tot;
        dij_f=sigma_c(1,1); s22 = integrate(omega, dij_f, qopt)/area_tot;
        
        if (d==3){
            dij_f=strain_rate(0,2); d13 = integrate(omega, dij_f, qopt)/area_tot;
            dij_f=strain_rate(1,2); d23 = integrate(omega, dij_f, qopt)/area_tot;
            dij_f=strain_rate(2,2); d33 = integrate(omega, dij_f, qopt)/area_tot;
            dij_f=sigma_c(0,2); s13 = integrate(omega, dij_f, qopt)/area_tot;
            dij_f=sigma_c(1,2); s23 = integrate(omega, dij_f, qopt)/area_tot;
            dij_f=sigma_c(2,2); s33 = integrate(omega, dij_f, qopt)/area_tot;
        }

        
        // compute macro strain rate on the cube
        uhi=uh[0]; d11m=(integrate(omega["left"],uhi,qopt)/area_LEFT-integrate(omega["right"],uhi,qopt)/area_RIGHT)/size_mesh[0];
        uhi=uh[1];d22m=(integrate(omega["top"],uhi,qopt)/area_TOP-integrate(omega["bottom"],uhi,qopt)/area_BOTTOM)/size_mesh[1];
        uhi=uh[1];d12m=(integrate(omega["left"],uhi,qopt)/area_LEFT-integrate(omega["right"],uhi,qopt)/area_RIGHT)/size_mesh[0];
        uhi=uh[0];d21m=(integrate(omega["top"],uhi,qopt)/area_TOP-integrate(omega["bottom"],uhi,qopt)/area_BOTTOM)/size_mesh[1];
        
        if (d==3){
            uhi=uh[2];d33m=(integrate(omega["front"],uhi,qopt)/area_FRONT-integrate(omega["back"],uhi,qopt)/area_BACK)/size_mesh[2];
            uhi=uh[0];d13m=(integrate(omega["front"],uhi,qopt)/area_FRONT-integrate(omega["back"],uhi,qopt)/area_BACK)/size_mesh[2];
            uhi=uh[2];d31m=(integrate(omega["left"],uhi,qopt)/area_LEFT-integrate(omega["right"],uhi,qopt)/area_RIGHT)/size_mesh[0];
            uhi=uh[2];d23m=(integrate(omega["top"],uhi,qopt)/area_TOP-integrate(omega["bottom"],uhi,qopt)/area_BOTTOM)/size_mesh[1];
            uhi=uh[2];d23m=(integrate(omega["top"],uhi,qopt)/area_TOP-integrate(omega["bottom"],uhi,qopt)/area_BOTTOM)/size_mesh[1];
            uhi=uh[1];d32m=(integrate(omega["front"],uhi,qopt)/area_FRONT-integrate(omega["back"],uhi,qopt)/area_BACK)/size_mesh[2];
        }

        // compute force on top(t) and right(r) and front(f) surface
        point ft(0.,0.,0.),fr(0.,0.,0.),ff(0.,0.,0.);
        field ex(Vh,0), ey(Vh,0), ez(Vh,0);
        ex[0]=1; ey[1]=1; ez[2]=(1);
        field fsi;
        ft[0]=integrate(omega["top"],dot(sigma_c*ey,ex),qopt)/area_TOP;
        ft[1]=integrate(omega["top"],dot(sigma_c*ey,ey),qopt)/area_TOP;
        
        fr[0]=integrate(omega["right"],dot(sigma_c*ex,ex),qopt)/area_RIGHT;
        fr[1]=integrate(omega["right"],dot(sigma_c*ey,ey),qopt)/area_RIGHT;
        
        if (d==3){
            ft[2]=integrate(omega["top"],dot(sigma_c*ey,ez),qopt)/area_TOP;
            fr[2]=integrate(omega["right"],dot(sigma_c*ex,ez),qopt)/area_RIGHT;
            ff[0]=integrate(omega["front"],dot(sigma_c*ez,ex),qopt)/area_FRONT;
            ff[1]=integrate(omega["front"],dot(sigma_c*ez,ey),qopt)/area_FRONT;
            ff[2]=integrate(omega["front"],dot(sigma_c*ez,ez),qopt)/area_FRONT;
        }
        // compute force on bottom(bo) and left(l) and back(ba) surface
        point fbo(0.,0.,0.), fl(0.,0.,0.), fba(0.,0.,0.);
        fbo[0]=integrate(omega["bottom"],dot(sigma_c*-1.0*ey,ex),qopt)/area_BOTTOM;
        fbo[1]=integrate(omega["bottom"],dot(sigma_c*-1.0*ey,ey),qopt)/area_BOTTOM;
        fl[0]=integrate(omega["left"],dot(sigma_c*-1.0*ex,ex),qopt)/area_LEFT;
        fl[1]=integrate(omega["left"],dot(sigma_c*-1.0*ex,ey),qopt)/area_LEFT;
        
        if (d==3){
            fbo[2]=integrate(omega["bottom"],dot(sigma_c*-1.0*ey,ez),qopt)/area_BOTTOM;
            fl[2]=integrate(omega["left"],dot(sigma_c*-1.0*ex,ez),qopt)/area_LEFT;
            fba[0]=integrate(omega["back"],dot(sigma_c*-1.0*ez,ex),qopt)/area_BACK;
            fba[1]=integrate(omega["back"],dot(sigma_c*-1.0*ez,ey),qopt)/area_BACK;
            fba[2]=integrate(omega["back"],dot(sigma_c*-1.0*ez,ez),qopt)/area_BACK;
        }

        // print value in text file
        std::clock_t t_end = std::clock();
        Float d0 = 1000 * (t_end-t_start)/ CLOCKS_PER_SEC;

        file_time_all.open(data_simu["output"]["out_folder"].get<string>()+"time_step", "txt", io::app);
        file_time_all << t << ";" << k_c << ";" << k_nl << ";" << residual_S_c << ";" << average_trD << ";" 
                    << average_trS << ";" << average_normD << ";" << average_normS 
                    << ";" << d11 << ";" << d12 << ";" << d13 << ";" << d22 << ";" << d23 << ";" << d33
                    << ";" << s11 << ";" << s12 << ";" << s13 << ";" << s22 << ";" << s23 << ";" << s33
                    << ";" << d11m << ";" << d12m << ";" << d13m << ";" << d21m << ";" << d22m << ";" << d23m << ";" << d31m << ";" << d32m << ";" << d33m
                    << ";" << ft[0] << ";" << ft[1] << ";" << ft[2]
                    << ";" << fbo[0] << ";" << fbo[1] << ";" << fbo[2]
                    << ";" << fr[0] << ";" << fr[1] << ";" << fr[2]
                    << ";" << fl[0] << ";" << fl[1] << ";" << fl[2]
                    << ";" << ff[0] << ";" << ff[1] << ";" << ff[2]
                    << ";" << fba[0] << ";" << fba[1] << ";" << fba[2]
                    << ";" << d0 ;
        file_time_all.close();
        file_simu.close();

        // backup to restart simulation
        backup.open(backup_file, "field");
        backup << catchmark("t") << t << endl
            << catchmark("uh") << uh
            << catchmark("ph") << ph
            << catchmark("c_axis") << c_axis
            << catchmark("c_axis_1") << c_axis_1 
            << catchmark("c_axis_2") << c_axis_2;
        backup.close();

    }while (t<data_simu["time_step"]["nb_t"].get<int>() && !data_simu["time_step"]["no_time"].get<bool>());
}

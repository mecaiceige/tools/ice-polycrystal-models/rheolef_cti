tuple<field,field> c_axis_BDF2(const field & uh,const field & c_axis_1,const field & c_axis_2,const field & c_axis_kc_1,const space& Sh,const space& Vh, const space& Th, const space& Mh,const nlohmann::json& j){
    
    size_t d = Vh.get_geo().dimension();
    
    field new_c_axis(Vh,0.0);
    
    field stress(Th,0.0);
    stress = stress_CTI_direct(uh,c_axis_kc_1,Sh,Th,Mh,j);
    field strain_rate(Th,0.0);
    strain_rate = interpolate(Th, D(uh));


    field c0(Vh,0.0);
    if (d==3){
        c0 = interpolate(Vh,compose(find_c0_3D,stress,c_axis_kc_1));  // WARNING : specific configuration a1=a2=a3 or a1=a2 or a2=a3 need to be written
    }
    else if (d==2){
        c0 = interpolate(Vh,compose(find_c0_2D,stress,c_axis_kc_1));
    };

    double Mo = j["time_step"]["mo"].get<double>();
    double dt = j["time_step"]["dt"].get<double>();
    double lambda = j["time_step"]["lambda"].get<double>();

    new_c_axis=interpolate(Vh,4./3.*c_axis_1-1./3*c_axis_2 +
                            + 2./3.*dt*(
                                ((grad(uh)-trans(grad(uh)))/2.0)*c_axis_kc_1
                                - lambda*(
                                    strain_rate*c_axis_kc_1
                                    -dot(c_axis_kc_1,strain_rate*c_axis_kc_1)*c_axis_kc_1
                                )
                                +Mo*(c0-c_axis_kc_1)
                            )
                          );


    new_c_axis = interpolate(Vh,new_c_axis/norm(new_c_axis));

    return make_tuple(new_c_axis, c0);    
}


tuple<field,field> c_axis_BDF1(const field & uh,const field & c_axis_1,const field & c_axis_kc_1,const space& Sh,const space& Vh, const space& Th, const space& Mh,const nlohmann::json& j){
    
    size_t d = Vh.get_geo().dimension();
    
    field new_c_axis(Vh,0.0);
    
    field stress(Th,0.0);
    stress = stress_CTI_direct(uh,c_axis_kc_1,Sh,Th,Mh,j);
    field strain_rate(Th,0.0);
    strain_rate = interpolate(Th, D(uh));


    field c0(Vh,0.0);
    if (d==3){
        c0 = interpolate(Vh,compose(find_c0_3D,stress,c_axis_1));  // WARNING : specific configuration a1=a2=a3 or a1=a2 or a2=a3 need to be written
    }
    else if (d==2){
        c0 = interpolate(Vh,compose(find_c0_2D,stress,c_axis_1));
    };

    double Mo = j["time_step"]["mo"].get<double>();
    double dt = j["time_step"]["dt"].get<double>();
    double lambda = j["time_step"]["lambda"].get<double>();

    new_c_axis=interpolate(Vh,c_axis_1 +
                            + dt*(
                                ((grad(uh)-trans(grad(uh)))/2.0)*c_axis_kc_1
                                - lambda*(
                                    strain_rate*c_axis_kc_1
                                    -dot(c_axis_kc_1,strain_rate*c_axis_kc_1)*c_axis_kc_1
                                )
                                +Mo*(c0-c_axis_kc_1)
                            )
                          );


    new_c_axis = interpolate(Vh,new_c_axis/norm(new_c_axis));

    return make_tuple(new_c_axis, c0);    
}
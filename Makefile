# Set the compiler
CXX = g++

# Set the flags for the compiler
CXXFLAGS = -std=c++11 -Wall -Wextra -pedantic -I/usr/lib/x86_64-linux-gnu/

# Set the include paths for the Rheolef headers
INCLUDES_RHEOLEF = $(shell rheolef-config --includes)

# Set the library files for Rheolef
LIBS_RHEOLEF = $(shell rheolef-config --libs)

# Set the output directory for the binary file
BINDIR = bin

# Set the target name
TARGET = $(BINDIR)/R3iCe

# Set the source files
SRCS = src/R3iCe.cc

# Set the object files
OBJS = $(SRCS:.cpp=.o)

# Build the binary
$(TARGET): $(OBJS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDES_RHEOLEF) -o $@ $^ $(LIBS_RHEOLEF)

# Build the object files
%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDES_RHEOLEF) -c -o $@ $<

# Install the binary to /usr/local/bin
install:
	install -m 755 $(TARGET) /usr/local/bin

# Clean the object files and the binary
clean:
	rm -f $(TARGET)

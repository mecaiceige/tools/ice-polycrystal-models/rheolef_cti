# R³iCe

<img src="logo_R3iCe.png" alt="R3iCe" width="200"/>

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)



**R**ecrystallization-**R**heology-**R**heolef **i**n **C**ontinous Transverse Isotropic mat**e**rial


This repository provides `R3iCe` code in the `src\` folder. But also a `cti_python` set of function in order to provide first data visualization in order to provide a quick analysis of the simulation.

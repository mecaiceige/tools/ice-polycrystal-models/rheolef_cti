import meshio
import xarray as xr
import xarrayuvecs.uvecs as xu
import pandas as pd
import numpy as np
import os
import glob
import natsort
from tqdm.notebook import trange
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt

def load_vector_P0(vector_name,mod=0):
    all_file=natsort.natsorted(glob.glob('vector_P0*.vtk'))
    data=[]
    if mod == 0:
        file=all_file
    else:
        file=all_file[::mod]

    for ff in file:
        v_xyz=meshio.read(ff).cell_data[vector_name][0]
        v_theta=np.arctan2(v_xyz[:,1],v_xyz[:,0])
        v_phi=np.arccos(v_xyz[:,2])
        data.append(np.dstack([v_theta,v_phi]))

    return xr.DataArray(np.stack(data),dims=['time','y','x','uvec'])

def play_c(data,res=100,**kwarg):
    fig = make_subplots()
    vis=True
    for i in trange(len(data.time)):
        figi=data[i,...].uvecs.plotly_ODF(bw=0.1,res=res,**kwarg)
        figi.data[2].visible=vis
        vis=False
        
        fig.add_trace(figi.data[2])

    fig.update_layout(figi.layout)
    # Create and add slider
    steps = []
    for i in range(int(len(fig.data))):
        step = dict(
            method="update",
            args=[{"visible": [False] * int(len(fig.data))},
                  {"title": "Time step: " + str(i)}],  # layout attribute
        )
        step["args"][0]["visible"][i] = True  # Toggle i'th trace to "visible"
        steps.append(step)


    sliders = [dict(
        active=0,
        currentvalue={"prefix": "Time step: "},
        steps=steps
    )]

    fig.update_layout(
        sliders=sliders
    )


    return fig

def compute_OT(data):
    nt=len(data.time)
    a1=np.zeros([nt])
    a2=np.zeros([nt])
    a3=np.zeros([nt])
    for i in range(nt):
        a,v=data[i,...].uvecs.OT2nd()
        a1[i]=a[0]
        a2[i]=a[1]
        a3[i]=a[2]

    return pd.DataFrame({'a1':a1,'a2':a2,'a3':a3})

def load_tensor_P1(adr,tensor_name,mod=0):
    all_file=natsort.natsorted(glob.glob(adr+'tensor_P1*.vtk'))
    data=[]
    if mod == 0:
        file=all_file
    else:
        file=all_file[::mod]

    for ff in file:
        data_t=meshio.read(ff).point_data[tensor_name]
        data_xr=np.zeros([1,data_t.shape[0],6])
        data_xr[0,:,0]=data_t[:,0,0]
        data_xr[0,:,1]=data_t[:,1,1]
        data_xr[0,:,2]=data_t[:,2,2]
        data_xr[0,:,3]=data_t[:,0,1]
        data_xr[0,:,4]=data_t[:,0,2]
        data_xr[0,:,5]=data_t[:,1,2]
        
        data.append(data_xr)

    return xr.DataArray(np.stack(data),dims=['time','y','x','sT'])


def build_video(data,metadata,res=200,**kwarg):
    os.mkdir("img_video/")
    vis=True
    figi=[]
    for i in trange(len(data.time)):
        tfig=data[i,...].uvecs.plotly_ODF(bw=0.1,res=res,**kwarg)
        tfig.update_layout(title_text="Time : "+str(i*metadata['output']['out_modulo']))
        tfig.write_image('img_video/frame_'+str(i)+'.png')
        
    os.system("ffmpeg -f image2 -r 3 -i ./img_video/frame_%01d.png -vcodec h264 -crf 0 -y texture_evolution.mp4")

def build_video_plt(data,metadata,res=200,**kwarg):
    os.mkdir("img_video/")
    vis=True
    figi=[]
    for i in trange(len(data.time)):
        plt.figure(figsize=(10,10))        
        data[i,...].uvecs.plotODF(bw=0.1,res=res,angle=np.array([]),**kwarg)
        plt.title("Time : "+str(i*metadata['output']['out_modulo']))
        plt.savefig('img_video/frame_'+str(i)+'.png', loc='left')
        
    os.system("ffmpeg -f image2 -r 3 -i ./img_video/frame_%01d.png -vcodec h264 -crf 0 -y texture_evolution.mp4")
        

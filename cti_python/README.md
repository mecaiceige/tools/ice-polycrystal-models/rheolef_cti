# cti_python

`cti_python` provides some python function in order to have a quick visualization of the simulation performed. It is currently under development.

## Install conda environnement 

We recommend to use [`mamba`](https://mamba.readthedocs.io/en/latest/user_guide/mamba.html) for faster install.

```
mamba env create -f environement.yml
conda activate base_cti
pip install -e .
```

## Change the path in 

```
cd cti_run_summary
sed -i "s|PATH|$(pwd)|g" cti_summary.sh
```

## Add script to `.bashrc`

Add to `.bashrc`

```
export PATH="<my_path_to>/rheolef_cti/cti_python/cti_run_summary:$PATH"
```

## Build report 

You can build report by running the script in the output folder of your simulation.

```
cti_summary.sh
```
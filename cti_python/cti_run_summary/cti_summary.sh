#!/bin/bash

source /home/chauvet/miniforge3/etc/profile.d/conda.sh
conda activate base_cti

pp=/home/chauvet/Documents/Projets/Gricad_Dev/rheolef_cti/cti_python/cti_run_summary

cp $pp/generate_summary.ipynb .

jupyter nbconvert --execute --to html generate_summary.ipynb --TagRemovePreprocessor.remove_input_tags 'remove-cell' --TagRemovePreprocessor.remove_input_tags 'remove-input' --TagRemovePreprocessor.remove_all_outputs_tags 'remove-cell'

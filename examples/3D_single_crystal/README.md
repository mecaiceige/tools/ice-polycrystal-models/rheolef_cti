# 3D single crystal without recrystallization

This example shows how to run a 3D simulation solving non linear CTI without c axis evolution. It can be used to calibrate the CTI parameter on single crystal experiment.

## From gmsh geometry file to mesh for `CTI` code

`single_crystal.mshcad` is a geometry file editable with [Gmsh](https://gmsh.info/).

It is need to create a mesh compatible with `CTI` code with :

```shell
gmsh -3 single_crystal.mshcad -format msh2 -o tmp_single_crystal.msh -clscale 1
msh2geo tmp_single_crystal.msh > single_crystal.geo
rm tmp_single_crystal.msh
```

# Prescribe crystal orientation

The file `orientation.rhori` provide the orientation of the single crystal.

The `rhori` file is structured using Euler angles (radians) in Bunge convention. Some direction a given in the table bellow :

| Physical volume name    | $\phi_1$     | $\phi$ | Corresponding direction (not in `rhori` file)|
|:--------------:|:-----------:|:------------:|:-----------:|
| G1 | $0$      | $0$        | z |
| G1 | $0$      | $\frac{\pi}{2}$        | y |
| G1 | $\frac{\pi}{2}$      | $-\frac{\pi}{2}$        | x |
| G1 | 0      | $\theta$        | $\left(0, sin(\theta) ,cos(\theta)\right)$ |

## Execute

```shell
R3iCe -j metadata.json
```

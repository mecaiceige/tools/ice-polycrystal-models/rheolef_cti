# 2D example for non linear CTI with recrystallization

This example shows how to run a 2D simulation solving CTI and c axis evolution with recrystallization.

## Sample configuration

In this example random orientation are selected within a uniform ODF for each elements of the `rh_mesh_2D_s1.geo` file. Then they are interpolated on the solving mesh `rh_mesh_2D_s2.geo`.

To do this we need to generate the mesh form the `box_111.mshcad` file.

```{shell}
gmsh -2 square_11.mshcad -format msh2 -o tmp_1.msh -clscale 0.5
msh2geo tmp_1.msh > rh_mesh_2D_s1.geo
rm tmp_1.msh

gmsh -2 square_11.mshcad -format msh2 -o tmp_2.msh -clscale 0.2
msh2geo tmp_2.msh > rh_mesh_2D_s2.geo
rm tmp_2.msh
```

## Execute

```shell
R3iCe --json metadata.json
```

